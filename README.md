Ryan Emch
Undergraduate of Computer Science
CS4200_SP23_Assignment10


Part 1.

a. This assignment took several days, about 6 in total.

b. The most challenging part was the last question on the TD algorithm. I think it would have been more helpful if we went over more of this during class.

c. Uncertainty is not knowing the direction of which an object will go first. If every space around it is safe, it can have equal probability that it will choose to go up, down, left or right.

d. So RL is a set of methods that learn "how to (optimally) behave" in an environment, whereas MDP is a formal representation of such environment.